import { AuthError } from 'expo-auth-session';
import { primary, white } from './Colors';

export default {
  buttonContainer: {
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    elevation: 0,
    backgroundColor: primary,
    borderRadius: 3,
  },
  DropdownWdith : {
width : 300
  },
  buttonLabel: {
    flex: 1,
    color: white,
    fontSize: 14,
    textAlign: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  row: {
    flexDirection: "row",
    flexWrap: "wrap",
    margin : 5,
    marginBottom : 3
  },
  Text : {
    margin : 8
  },
  loginBtn: {
    width: "80%",
    borderRadius: 0,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "#4081EC",
  },
  gridStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: 'center',
  },
  sigininclr: {
    color: "#FFFFFF",
    fontSize: 25,
    marginRight: 10
  },
  textInput : {
  
      height: 40,
      width : 220,
      marginTop: 10,
      // margin: 12,
      borderWidth: 1,
      padding: 10,
  },
  Button: {
    textAlign: 'center',
    marginVertical: 8,
    width: 80,
  },
  Card: {
    margin:'5%',
    width:'100%',
  },
  container3: {
    flex: 1,
    backgroundColor: "white",
  },
  mainCardView: {
    height: "auto",
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "white",
    borderRadius: 15,
    shadowColor: "gray",
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 1,
    shadowRadius: 8,
    elevation: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    marginTop: 6,
    marginBottom: 6,
    marginLeft: 16,
    marginRight: 16,
  },
  subCardView: {
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: "red",
    borderColor: "gray",
    borderWidth: 1,
    borderStyle: 'solid',
    alignItems: 'center',
    justifyContent: 'center',
  },
};



