import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from "../Styles/styles";
import { DynamicForm } from "../Components/DynamicForm";
// import { ReadFileData } from "../../Service/backendapi";

export default DisplayForm = ({ navigation }) => {

  const [localStorVal, setLocalStorVal] = useState("");
  // get localStorage data
  const getLocalValue = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Key');

      if (value !== null) {
        setLocalStorVal(value);
      }
    } catch (e) {
      console.log(e, "error")
    }
  }


  useEffect(() => {
    getLocalValue();
  }, [])

  return (
    <DynamicForm filename={localStorVal} />
  );
}

