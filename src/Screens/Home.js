import React, { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    FlatList,
    Button,
    ScrollView,
    SafeAreaView
} from "react-native";
import axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AntDesign } from '@expo/vector-icons';
import * as DocumentPicker from 'expo-document-picker';
import * as FileSystem from 'expo-file-system';
import { apiUrl, BinaryUpload, upload, ReadDir } from "../../Service/Config/config";
import { ReadDirectory } from "../../Service/backendapi";

export default Home = ({ navigation }) => {
    const [form, setForm] = useState('');
    const [newFile, setNewFile] = useState(false);
    const [accessToken, SetAccessToken] = useState([]);

    const ACCESS_TOKEN = AsyncStorage.getItem('@Access-token').then((token) => {
        SetAccessToken(token);
    })

    console.log(accessToken, "accessToken");
    const Item = ({ item, onPress }) => (
        <TouchableOpacity onPress={onPress} style={[styles.item]} key={item}>
            <Text style={[styles.title]}>{item}</Text>
        </TouchableOpacity>
    );
    const renderItem = ({ item }) => {

        return (
            <Item
                item={item}
                onPress={() => {
                    storeData(item);
                }}
            />
        );
    };




    //   set localStorage Data

    const storeData = async (value) => {

        try {
            await AsyncStorage.setItem('@storage_Key', value);
            navigation.navigate('Form');
        } catch (e) {
            console.log(e, "error")
        }
    }

    //   readDir api call
    let DispalyAllFiles = async () => {
        console.log("displayFIles");
        await ReadDirectory().then((res) => {
            console.log(res.data, "response");
            setForm(res.data.filenames);
        }).catch((error) => {
            console.log(error, "Homeerror");
        })
    }

    const getFile = async () => {
        const res = await DocumentPicker.getDocumentAsync({
            type: 'application/json',
        });

        if (res.type === "success") {
            const gifDir = FileSystem.documentDirectory + 'json/';
            const gifFileUri = () => gifDir + res.name;

            const dirInfo = await FileSystem.getInfoAsync(gifDir);

            if (!dirInfo.exists) {

                await FileSystem.makeDirectoryAsync(gifDir, { intermediates: true });
            }
            try {
                const response = await FileSystem.uploadAsync(
                    `${apiUrl}${upload}`,
                    res.uri,
                    {
                        fieldName: "files",
                        httpMethod: "POST",
                        uploadType: FileSystem.FileSystemUploadType.MULTIPART,
                    }
                )
                // setNewFile(true);
                if (response.status === 200) {
                    setNewFile(true);
                    const Object = JSON.stringify({
                        properties: {
                            title: 'newSpreadSheet',
                        },
                    });

                    // create new spreadsheet
                    axios.post("https://sheets.googleapis.com/v4/spreadsheets", Object, {
                        headers: {
                            'Content-Type': 'application/json',
                            "Authorization": `Bearer ${accessToken}`,
                        }
                    }
                    )
                        .then((res) => {
                            console.log(res.data, "responseaxios")
                        }).catch((error) => {
                            console.log(error, "erroraxios")
                        })
                }
                else {
                    setNewFile(false);
                    console.log("error");
                }

            }

            catch (error) {
                console.log(error, "error")
            }
        } else {
            console.log(res.type);
        }
    };

    useEffect(() => {
        DispalyAllFiles();
    }, [newFile])
    console.log(newFile, "newFilestatus");
    return (
        <View>
            <ScrollView keyboardDismissMode="interactive">
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={form}
                        renderItem={renderItem}
                        keyExtractor={item => item}
                    // extraData={selectedId}
                    // refreshControl={
                    //     <RefreshControl refreshing={refreshing} onRefresh={loadUserData} />
                    //   }
                    />
                </SafeAreaView>
                <TouchableOpacity onPress={getFile}>
                    <AntDesign style={styles.plusicon} name="plus" size={40} color="black" />
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
    plusicon: {
        marginLeft: 330,
        marginTop: 30,
        marginBottom: 20
    },
    container: {
        flex: 1,
        marginTop: 0,
    },
    item: {
        backgroundColor: '#A8D1DF',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },

});


