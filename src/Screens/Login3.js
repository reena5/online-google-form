import * as React from 'react';
import * as WebBrowser from 'expo-web-browser';
import * as Google from 'expo-auth-session/providers/google';
import axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StyleSheet, View, Button, Text, TouchableOpacity, Image } from 'react-native';

WebBrowser.maybeCompleteAuthSession();

export default function LoginScreen2({ navigation }) {
  // const Scopes= "https://sheets.googleapis.com/v4/spreadsheets";
  const [request, response, promptAsync] = Google.useAuthRequest({
    expoClientId: '779217634616-nlvjl7va1uca5nma5m2kquntlh4jrgq9.apps.googleusercontent.com',
    scopes: ["https://www.googleapis.com/auth/spreadsheets"],
  });
  React.useEffect(() => {
    if (response?.type === 'success') {
      console.log(response, "responseLogin");
      AsyncStorage.setItem('@Access-token', response.authentication.accessToken);

      axios.get('https://www.googleapis.com/oauth2/v3/userinfo?access_token=' + response.authentication.accessToken)
        .then((res) => {
          console.log(res.data, "LoginResponse");
        })

      // console.log(response, "response");
      navigation.navigate('Home');
    }
  }, [response]);

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.loginBtn} onPress={() => {
        promptAsync()
      }
      }>
        <View style={styles.gridStyle}>
          <Image style={styles.image} source={require("../../assets/Googlelogog.png")} />
          <Text style={styles.sigininclr}>Sign in with Google</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  loginBtn: {
    width: "80%",
    borderRadius: 0,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#4081EC",
  },
  image: {
    width: 40,
    height: 40,
    marginRight: 25
  },
  gridStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: 'center',
  },
  sigininclr: {
    color: "#FFFFFF",
    fontSize: 25,
    marginRight: 10
  }
});