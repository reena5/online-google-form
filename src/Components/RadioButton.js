import React, { useState } from 'react';
// import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RadioButton } from 'react-native-paper';
import styles from '../Styles/styles';

export const DynamicRadioButton = ({ itemData, handleRadioValue }) => {
  const [checked, setChecked] = useState('');
  const [newChecked, setNewChecked] = useState();
  console.log(checked, "checked")
  const handleRadioVal = (text) => {

    const newValue = {
      title: itemData.title,
      value: text
    }
    setNewChecked(newValue);
  };

  handleRadioValue(newChecked);
  return (
    <View style={styles.mainCardView}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ marginLeft: 12 }}>
          <Text
            style={{
              fontSize: 14,
              color: "black",
              fontWeight: 'bold',
              textTransform: 'capitalize',
            }}>
            {itemData.title}
          </Text>
          <View
            style={{
              marginTop: 6,
              borderWidth: 0,
              width: '100%',
            }}>
            <View style={styles.row}>
              {itemData.options.map((item) => {

                return (
                  <>
                    <View style={styles.row}>
                      <Text >{item.label}</Text>
                    </View>
                    <View >
                      <RadioButton
                        value={item.label}
                        status={item.label === checked ? 'checked' : 'unchecked'}
                        onPress={() => {
                      setChecked(item.label);
                      handleRadioVal(item.label);
                        // console.log("pressed");
                      }}
                    />
                    </View>


                  </>
                )
              })}
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}
