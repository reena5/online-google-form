import React from 'react';
import { Checkbox } from 'react-native-paper';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import styles from '../Styles/styles';

export const DynamicCheckbox = ({ itemData, handleCheckBoxValue }) => {
  const [checked, setChecked] = React.useState("");
  const [newChecked, setNewChecked] = React.useState();
  const handleCheckBoxVal = (text) => {
    const newValue = {
      title: itemData.title,
      value: text
    }
    setNewChecked(newValue);
  };

  handleCheckBoxValue(newChecked);


  return (
    <View style={styles.mainCardView}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ marginLeft: 12 }}>
          <Text
            style={{
              fontSize: 14,
              color: "black",
              fontWeight: 'bold',
              textTransform: 'capitalize',
            }}>
            {itemData.title}
          </Text>
          <View
            style={{
              marginTop: 6,
              borderWidth: 0,
              width: '100%',
            }}>
            <View style={styles.row}>
              {itemData.options.map((item) => {
                return (
                  <>
                    <View style={styles.row}>
                      <Text >{item.label}</Text>
                    </View>
                    <View >
                      <Checkbox
                        status={item.label === checked ? 'checked' : 'unchecked'}
                        onPress={() => {
                          setChecked(item.label);
                          handleCheckBoxVal(item.label);
                          // setChecked(item.label) 
                        }}
                      />
                    </View>
                  </>
                )
              })}
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}

