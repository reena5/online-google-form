import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import styles from '../Styles/styles';

import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';

export const DynamicText = ({itemData}) => {
  
  return (
    <View style={styles.mainCardView}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginLeft: 12}}>
              <Text
                style={{
                  fontSize: 14,
                  color: "black",
                  fontWeight: 'bold',
                  textTransform: 'capitalize',
                }}>
                {itemData.title}
              </Text>
              <View
                style={{
                  marginTop: 4,
                  borderWidth: 0,
                  width: '85%',
                }}>
                <Text
                  style={{
                    color: "gray",
                    fontSize: 12,
                  }}>
                   {itemData.title}
                </Text>
              </View>
            </View>
          </View>
        </View>
  )
}