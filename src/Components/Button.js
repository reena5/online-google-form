import React, { useState } from 'react';
import { Image, StyleSheet, Text, Button, TouchableOpacity, View } from 'react-native';
import styles from '../Styles/styles';

export const DynamicButton = ({ itemData, handleButtonValue }) => {
  const [buttonVal, setButtonVal] = useState(false);
  const handleCheckBoxVal = () => {
    // setButtonVal(true);
    handleButtonValue(true);
  };


  return (
    <TouchableOpacity style={styles.loginBtn}>
      <View style={[styles.gridStyle, {
        // margin :10
      }]}>

        <Button
          style={styles.Button}
          title={itemData.label}
          onPress={handleCheckBoxVal}
        />
      </View>
    </TouchableOpacity>
  )
}