import React, { useEffect, useRef } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { handleTextInput } from 'react-native-formik';
import { TextInput } from 'react-native-paper';
import styles from '../Styles/styles';

export const DynamicTextInput = ({ itemData, handleTextInputField }) => {
  const textInputReference = useRef(null);
  const [textInput, setTextInput] = React.useState({
    title: "",
    value: ""
  });
  // const [updatedTextInputVal, setUpdateTextVal]  = React.useState()
console.log(textInputReference, "textInputReference")
  const handleTextInput = (text) => {
    // const isFocused = isFocused();
    // console.log(text.isFocused(), "isFocused");

    const newValue = {
      title: itemData.title,
      value: text
    }
    setTextInput(newValue);
    handleTextInputField(newValue);
  };

  // useEffect(() => {
  //   handleTextInputField(textInput);
  // }, [textInput])

  return (
    <View style={styles.mainCardView}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ marginLeft: 12 }}>
          <Text
            style={{
              fontSize: 14,
              color: "black",
              fontWeight: 'bold',
              textTransform: 'capitalize',
            }}>
            {itemData.title}
          </Text>
          <View
            style={{
              marginTop: 4,
              borderWidth: 0,
              width: '85%',
            }}>
            <TextInput
              ref={textInputReference}
              required
              onChangeText={(text) => handleTextInput(text)}
              value={textInput.value}
              mode="outlined"
              label={itemData.label}
              style={{ width: 250, margin: "auto" }}
            />
          </View>
        </View>
      </View>
    </View>
  )
}