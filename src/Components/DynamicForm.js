import React, { useState, useEffect } from 'react'
import { DynamicText } from "./Text";
import { DynamicTextInput } from "./TextInput";
import { DynamicSelect } from "./Select";
import { DynamicButton } from './Button';
import { DynamicRadioButton } from './RadioButton';
import { DynamicCheckbox } from './CheckBox'
import AsyncStorage from '@react-native-async-storage/async-storage';
// import { GoogleSheetApi } from '../../Service/backendapi';
// import axios from "axios";
import styles from "../Styles/styles"
import {
    View,
    ScrollView
} from "react-native";
import { ReadFileData } from "../../Service/backendapi";
import axios from 'axios';

export const DynamicForm = (props) => {
    const [dynamicContent, setDynamicContent] = useState();
    const arrayInputVal = [];
    const [accessToken, SetAccessToken] = useState([]);

    const ACCESS_TOKEN = AsyncStorage.getItem('@Access-token').then((token) => {
        SetAccessToken(token);
    })

    const handleTextInputField = (inputVal) => {
        console.log(inputVal, "data");
        if (inputVal) {
            arrayInputVal.push(inputVal);
        }
    }

    const handleSelectValue = (slectVal) => {
        console.log(slectVal, "slectVal")
        if (slectVal) {
            arrayInputVal.push(slectVal);
        }
        return slectVal
    }

    const handleButtonValue = async (buttonVal) => {
        if (buttonVal === true) {
            console.log(arrayInputVal, "arrayInputVal48");
            // arrayInputVal.map((item) => {
            //     GoogleSheetApi(item).then((res) => {
            //         console.log(res, "buttonResponse")
            //     }).catch((error) => {
            //         console.log(error, "buttonError")
            //     })
            // })
            
            const sheetID = "1N7jkJES4iDkZGTdSGYcwoJB8Duwi0eVKN_IwRkPs67s";
            const API_KEY = "AIzaSyAwWfKox3cf4oX8EsyaF-LjU2TdiAt7QH0";

            // axios method to get the data of spreadsheet
            const request = await axios.get(
                `https://sheets.googleapis.com/v4/spreadsheets/${sheetID}/values/A:Z?key=${API_KEY}`
            );
            console.log(request.data, "dataRequset");
            console.log(accessToken, "accessToken");

            // axios method to write the data to the sheetc
            arrayInputVal.map((item) => {

                // const body = JSON.stringify({
                //     "requests": [
                //         {
                //             "updateCells": {
                //                 "range": {
                //                     "startRowIndex": 0,
                //                     "startColumnIndex": 0,
                //                     "endColumnIndex": 1,
                //                     "endRowIndex": 3,
                //                     "sheetId": 0
                //                 },
                //                 " ": [
                //                     {
                //                         "values": item.value
                //                     },
                //                 ],
                //                 "fields": "*"
                //             }
                //         }
                //     ]

                // })
                axios.post(`https://sheets.googleapis.com/v4/spreadsheets/${sheetID}/values/A15:append`, {
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${accessToken}`
                    }
                }, {
                    "range": "A15",
                    "values": [
                        [
                            "Adnan1",
                            "Adnan2",
                            "Adnan3"
                        ]
                    ]
                })
                    .then((res) => {
                        console.log(res, "postResponse");
                    })
                    .catch((error) => {
                        console.log(error, "buttonError");
                    })
            })

            //     console.log(post, "post");

            // })

            //axios to create spreadsheet
            // axios.post("https://sheets.googleapis.com/v4/spreadsheets", {
            //     headers: {
            //         'Content-Type': 'application/json',
            //         "Authorization": `${accessToken}`,
            //     }
            // },
            //     Object
            // )
            //     .then((res) => {
            //         console.log(res.data, "responseaxios")
            //     }).catch((error) => {
            //         console.log(error, "erroraxios")
            //     })
            // console.log(accessToken, "accessToken");
            // try {
            // const response = await fetch(
            //     `https://sheets.googleapis.com/v4/spreadsheets`,
            //     {
            //         method: 'POST',
            //         headers: {
            //             'Content-Type': 'application/json',
            //             "Authorization": `Bearer ${accessToken}`,
            //         },
            //         body: JSON.stringify({
            //             properties: {
            //                 title: 'newSpreadSheetTest',
            //             },
            //         }),
            //     }
            // )
            // .then((res) =>
            // {
            //     console.log(res.json(), "consoleresponse")
            // })
            // .catch ((error) => {
            //     console.log(error, "authError");
            // })
            // const body = response.json();
            // console.log(body, "consoleresponse");
            // }
            // catch (error) {
            //     console.log(error, "authError");
            // }
        }
    }

    const handleRadioValue = (radioVal) => {
        console.log(radioVal, "radioVal")
        if (radioVal) {
            arrayInputVal.push(radioVal);
        }

    }
    const handleCheckBoxValue = (checkboxVal) => {
        console.log(checkboxVal, "checkboxVal")
        if (checkboxVal) {
            arrayInputVal.push(checkboxVal);
        }
    }

    const Form = () => {
        return dynamicContent?.content.map(function (item, index) {

            if (item.type === "text") {
                return <DynamicText key={item.title} itemData={item} style={styles.Text} />
            }
            else if (item.type === "textInput") {

                return <DynamicTextInput key={item.title} itemData={item} style={styles.Text} handleTextInputField={handleTextInputField} />
            }
            else if (item.type === "select") {
                return <DynamicSelect key={item.title} itemData={item} style={styles.Text} handleSelectValue={handleSelectValue} />
            }
            else if (item.type === "button") {
                return <DynamicButton key={item.title} itemData={item} style={styles.Text} handleButtonValue={handleButtonValue} />
            }
            else if (item.type === "radio") {
                return <DynamicRadioButton key={item.title} itemData={item} handleRadioValue={handleRadioValue} />
            }
            else if (item.type === "checkbox") {
                return <DynamicCheckbox key={item.title} itemData={item} handleCheckBoxValue={handleCheckBoxValue} />
            }
        });
    }

    useEffect(() => {
        const value = props;
        ReadFileData(value)
            .then((res) => {
                setDynamicContent(res.data);
            })
            .catch((error) => {
                console.log(error);
            })
    }, [props])

    return (
        dynamicContent?.content ?
            <ScrollView keyboardDismissMode="interactive">
                <Form />
            </ScrollView>
            : <View></View>
    )
}
