import React, {useState} from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import SelectDropdown from 'react-native-select-dropdown';
import styles from '../Styles/styles';

export const DynamicSelect = ({itemData, handleSelectValue, handleNewSelect}) => {
const [selectVal, setSelectVal]= useState();
  const handleSelectVal = (text) => {
    const newValue = {
      title: itemData.title,
      value: text
    }
    setSelectVal(newValue);
    handleSelectValue(newValue);
  };

  

  return (
	<View style={styles.mainCardView}>
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <View style={{marginLeft: 12}}>
        <Text
          style={{
            fontSize: 14,
            color: "black",
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}>
          {itemData.title}
        </Text>
		<View
          style={{
            marginTop: 6,
            borderWidth: 0,
            width: '100%',
          }}>
    <SelectDropdown
	dropdownStyle={styles.DropdownWdith}
	data={itemData.options}
	onSelect={(selectedItem, index) => {
		console.log(selectedItem, index)
    handleSelectVal(selectedItem)
   
	}}
	buttonTextAfterSelection={(selectedItem, index) => {
		return selectedItem
	}}
	rowTextForSelection={(item, index) => {
		return item
	}}

/>
</View>
</View>
</View>
</View>
  )
}
