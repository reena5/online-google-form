import axios from 'axios';
import { GoogleSheet } from './Config/config';
import { apiUrl, upload, ReadDir, ReadFile, DeleteFile } from './Config/config';

export const ReadDirectory = async () => {
  let url = `${apiUrl}${ReadDir}`;
  return await axios.post(url).then((res) =>
  {
    return res
  }).catch((error) => {
    return error
  })
};

export const ReadFileData = async (value) => {
  let url = `${apiUrl}${ReadFile}`;
  return await axios.post(url, value).then((res) => {
    
    return res
  }).catch((error) => {
   
    return error
  })
};

export const GoogleSheetApi = async(arrayInputVal) =>
{
  console.log(arrayInputVal.value, "arrayInputValapi");
  const object = {
    Email : "email",
    Password : "password",
    Designation : "designation",
  }
  await axios.post(GoogleSheet, object).then((res) =>
  {
    console.log(res);
    return res
  })
  .catch((err) =>
  {
    console.log(err, "error");
    return err
  })
}

