const apiUrl = "https://bb90-103-5-134-42.in.ngrok.io";
const BinaryUpload = "/binary-upload";
const upload = "/upload";
const ReadDir = "/readDir";
const ReadFile = "/readFile";
const DeleteFile = "/delete";
const GoogleSheet = 'https://sheet.best/api/sheets/e1553836-ff78-4e3a-acb6-317167570a2e';
export { apiUrl, BinaryUpload, upload, ReadDir, ReadFile, DeleteFile, GoogleSheet };
