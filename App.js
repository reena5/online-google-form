import 'react-native-gesture-handler';
import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import Home from './src/Screens/Home';
import DisplayForm from './src/Screens/DisplayForm';
import LoginScreen2 from './src/Screens/Login3';

function Feed({ navigation }) {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Feed Screen</Text>
      <Button title="Open drawer" onPress={() => navigation.openDrawer()} />
      <Button title="Toggle drawer" onPress={() => navigation.toggleDrawer()} />
    </View>
  );
}

function Notifications() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Notifications Screen</Text>
    </View>
  );
}

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem
        label="Close drawer"
        onPress={() => props.navigation.closeDrawer()}
      />
      <DrawerItem
        label="Toggle drawer"
        onPress={() => props.navigation.toggleDrawer()}
      />
    </DrawerContentScrollView>
  );
}

const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();



function MyDrawer() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Feed" component={Feed} />
      <Drawer.Screen name="Notifications" component={Notifications} />
    </Drawer.Navigator>
  );
}

function HomeScreen() {
  return (
    <Stack.Navigator
    screenOptions={{
      mode: "card",
    }}
    >
      <Stack.Screen
        name="home"
        component={Home}
      />
       {/* <Stack.Screen
        name="Form"
        component={DisplayForm}
      /> */}
      </Stack.Navigator>
  );
}
function FormScreen() {
  return (
    <Stack.Navigator
    screenOptions={{
      mode: "card",
    }}
    >
       <Stack.Screen
        name="form"
        component={DisplayForm}
      />
      </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
            <Stack.Navigator
            screenOptions={{
              mode: "card",
              headerShown: false,
            }}
            >
              <Stack.Screen
                name="login"
                component={LoginScreen2}
              />
              <Stack.Screen
                name="Home"
                component={HomeScreen}
              />   
                  <Stack.Screen
                name="Form"
                component={FormScreen}
              />        
            </Stack.Navigator>
    </NavigationContainer>
  );
}

